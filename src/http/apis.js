import http from './index'
import store from '../store'
import {
    SET_INFO
} from '../store/mutation-type'
import router from '../router'
export default {
    //登录
    login: (data, message) => {
        http('login', data, 'POST')
            .then(({
                meta: {
                    msg

                },
                data: {
                    token,
                    username
                }
            }) => {
                message({
                    type: 'success',
                    message: msg
                })
                store.commit(SET_INFO, {
                    token,
                    username
                })
                router.push({
                    name: 'Home'
                })
            }).catch(err => {
                message({
                    type: 'error',
                    message: err
                })
                message({
                    type: 'info',
                    message: '取消删除'
                  });
            })
    },
    // 获取左侧权限菜单
    async getMenus() {
        const res = await http('menus', null, 'GET')
        return res.data;
    },
    async getusers(obj) {
        const res = await http('users', null, 'GET', obj)
        return res.data;
    },
    async userstate(inof, message) {
        const res = await http(inof, null, 'PUT')
        if (res.meta.status == 200) {

            message({
                type: 'success',
                message: res.meta.msg
            })
        }


        return res;
    },
    async addclick(obj, message) {
        const res = await http('users', obj, 'POST')
        if (res.meta.status === 201) {
            message({
                type: 'success',
                message: res.meta.msg
            })
        }

        return res;
    },

    async xiugai(id,message) {
        const res = await http('users/'+id, null, 'GET')
        if(res.meta.status==200){
            message({
                type: 'success',
                message: res.meta.msg
            })
        }
        return res;
    },

    async addvisibleclick(id,data,message) {
        const res = await http(
        'users/'+id,
        
        data, 'PUT')
        if(res.meta.status==200){
            message({
                type: 'success',
                message: res.meta.msg
            })
        }
        return res;
    },

    async shanchu(id,message) {
        const res = await http('users/'+id, null, 'delete')
        if(res.meta.status==200){
            message({
                type: 'success',
                message: res.meta.msg
            })
        }
        
        return res;
    },

}