import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css'
import apis from './http/apis'
import ElementUI from 'element-ui';
import { Message } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import http from './http'
import './assets/global.css'
import 'default-passive-events'


Vue.prototype.$http = http
Vue.config.productionTip = false
Vue.prototype.$apis = apis

Vue.prototype.$ELEMENT = {
  size: 'small',
  zIndex: 3000
};
Vue.use(ElementUI);
new Vue({
  router,
  store,
  Message,
  // Breadcrumb,
  render: h => h(App)
}).$mount('#app')
