import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import login from '../components/login.vue'
import users from '../components/users.vue'
import roles from '../components/roles.vue'
import rights from '../components/rights.vue'

const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
return routerPush.call(this, location).catch(error=> error)
}
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect:{
      name:'login'
    }
  },
  {
    path: '/login',
    name: 'login',
    component:login
  },
  {
    path: '/Home',
    name: 'Home',
    component:Home,
    redirect:'/users',
    children:[
      {
      path:'/users',
      name:'users',
      component:users,
      // children:[
      //   {
      //     path:'/admin',
      //     name:'admin',
      //     component:admin,
      //   }
      // ]
    },
    {
      path:'/roles',
      name:'roles',
      component:roles
    },
    {
      path:'/rights',
      name:'rights',
      component:rights
    }
  ]
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
